let types = ../dhall-gitlab/types.dhall

let defaults = ../dhall-gitlab/defaults.dhall

let Stage
    : Type
    = Text

let ansible = "ansible" : Stage

let runAnsible =
        defaults.Job
      ⫽ { name = "run_ansible" }
      ⫽ { stage = ansible }
      ⫽ { script = types.Script.Command "ansible-playbook prepare.yml" }

let prodAnsible =
        defaults.Job
      ⫽ { name = "prod_ansible" }
      ⫽ { stage = ansible }
      ⫽ { script = types.Script.Command "ansible-playbook prod.yml" }
      ⫽ { tags = [ "tag" ] }

in  { stages = [ ansible ]
    , run_ansible = runAnsible
    , prod_ansible = prodAnsible
    }
