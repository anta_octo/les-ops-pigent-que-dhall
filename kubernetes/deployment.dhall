let kubernetes =
      https://raw.githubusercontent.com/dhall-lang/dhall-kubernetes/6a47bd50c4d3984a13570ea62382a3ad4a9919a4/1.16/package.dhall sha256:ab1c971ddeb178c1cfc5e749b211b4fe6fdb6fa1b68b10de62aeb543efcd60b3

let deployment =
      kubernetes.Deployment::{
      , metadata = kubernetes.ObjectMeta::{ name = "nginx" }
      , spec = Some kubernetes.DeploymentSpec::{
        , replicas = Some 2
        , revisionHistoryLimit = Some 10
        , selector = kubernetes.LabelSelector::{
          , matchLabels = toMap { app = "nginx" }
          }
        , strategy = Some kubernetes.DeploymentStrategy::{
          , type = Some "RollingUpdate"
          , rollingUpdate =
              { maxSurge = Some (kubernetes.IntOrString.Int 5)
              , maxUnavailable = Some (kubernetes.IntOrString.Int 0)
              }
          }
        , template = kubernetes.PodTemplateSpec::{
          , metadata = kubernetes.ObjectMeta::{
            , name = "nginx"
            , labels = toMap { app = "nginx" }
            }
          , spec = Some kubernetes.PodSpec::{
            , containers =
              [ kubernetes.Container::{
                , name = "nginx"
                , image = Some "nginx:1.15.3"
                , imagePullPolicy = Some "Always"
                , ports = [ kubernetes.ContainerPort::{ containerPort = 80 } ]
                , resources = Some
                    { limits = toMap { cpu = "500m" }
                    , requests = toMap { cpu = "10m" }
                    }
                }
              ]
            }
          }
        }
      }

in  deployment
